package com.company;

import java.util.*;

public class Turnstile {

    int number = 1000;

    public static void main(String[] args) {
        Turnstile turnstile = new Turnstile();

        Scanner scan = new Scanner(System.in);
        Random random = new Random();

        Parking parking = new Parking();
        int n = parking.n;
        int k = parking.k;

        for (int i = 0; i < n; i++) {
          parking.placeList.add(i,0);
        }
        for (int i = 0; i < k; i++) {
            parking.placeBigList.add(i,0);
        }


        int time;

        while (true) {

            carsLeaving(parking.placeList, n);
            carsLeaving(parking.placeBigList, k);

            int carCounter = random.nextInt((n/2));
            System.out.println("New cars arrived:" + carCounter);
            Cars cars = new Cars();

            int truckCounter = random.nextInt((n/3));
            System.out.println("New trucks arrived:" + truckCounter);
            Cars truck = new Cars();

            boolean occupied;

            occupied = placementOfCars(parking.placeBigList, truckCounter, turnstile, truck, false);
            if (occupied) {
                System.out.println("Sorry, all the truck Parking spaces are occupied. Proceed to the car Park");

                int noapMethod = numberOfAvailablePlace(parking.placeList);
                if ((2 * truckCounter) <= noapMethod) {
                    for (int i = 0; i < truckCounter; i += 2) {
                        turnstile.setNumber(truck.getNumber(turnstile.getNumber()));
                        time = truck.getTime();

                        parking.placeList.set(i, turnstile.getNumber() * 1000 + time);
                        parking.placeList.set((i + 1),turnstile.getNumber() * 1000 + time);

                    }
                } else {
                    if (!((noapMethod == 0) | (noapMethod == 1))) {
                        if (noapMethod % 2 == 0) {
                            for (int i = 0; i < noapMethod; i += 2) {
                                turnstile.setNumber(truck.getNumber(turnstile.getNumber()));
                                time = truck.getTime();

                                parking.placeList.set(i, turnstile.getNumber() * 1000 + time);
                                parking.placeList.set((i + 1),turnstile.getNumber() * 1000 + time);
                            }
                        } else {
                            for (int i = 0; i < (noapMethod - 1); i += 2) {
                                turnstile.setNumber(truck.getNumber(turnstile.getNumber()));
                                time = truck.getTime();

                                parking.placeList.set(i, turnstile.getNumber() * 1000 + time);
                                parking.placeList.set((i + 1),turnstile.getNumber() * 1000 + time);
                            }
                        }
                    }
                    System.out.println("Sorry, there are no free truck Parking spaces.");
                    appearanceParkingSpaces(parking.placeList);
                }
            }

            occupied = placementOfCars(parking.placeList, carCounter,turnstile, cars,false);
            if (occupied) {
                System.out.println("Sorry, there are no free Parking spaces.");
                appearanceParkingSpaces(parking.placeList);
            }


            System.out.println("Do you want to see the command menu?");
            while (scan.next().equalsIgnoreCase("yes")) {
                System.out.println("If you want to know the current location of the machines, press 1.");
                System.out.println("If you want to know the available and occupied seats, as well as the waiting time, press 2.");
                System.out.println("Turning on the fire alarm. Press 3!");
                System.out.println("Finish the move and move on to the next one. Press 4");
                System.out.println("Detailed information about Parking. Press 5");

                switch (scan.nextInt()) {
                    case 1:
                        System.out.println("Truck:");
                        System.out.println();
                        enter(parking.placeBigList);
                        System.out.println();
                        System.out.println("Passenger car:");
                        enter(parking.placeList);
                        System.out.println();
                        break;

                    case 2:
                        System.out.println("Freely:");
                        System.out.println(numberOfAvailablePlace(parking.placeList));
                        System.out.println("Occupied:");
                        System.out.println(n - numberOfAvailablePlace(parking.placeList));
                        appearanceParkingSpaces(parking.placeList);
                        break;

                    case 3:
                        System.out.println("Attention, the fire alarm has gone off! Please leave the Parking lot!");
                        parking.placeList.clear();
                        parking.placeBigList.clear();
                        enter(parking.placeList);
                        enter(parking.placeBigList);
                        break;

                    case 4:
                        break;
                    case 5:
                        if((parking.placeList.get(0).equals(parking.placeList.get(1)))&(parking.placeList.get(0) != 0)){
                            System.out.println("Truck");
                            System.out.println("Number:" + (parking.placeList.get(0)/1000));
                            System.out.println("Parking time:"+(parking.placeList.get(0)%1000));
                        }else{
                            System.out.println("Passenger car:");
                            System.out.println("Number:" + (parking.placeList.get(0)/1000));
                            System.out.println("Parking time:"+(parking.placeList.get(0)%1000));
                        }
                        for (int i = 1; i < n-1; i++){
                            if((parking.placeList.get(i).equals(parking.placeList.get(i + 1)))&(parking.placeList.get(i) != 0)){
                                System.out.println("Truck");
                            }else{
                                if((parking.placeList.get(i).equals(parking.placeList.get(i - 1)))&(parking.placeList.get(i) != 0)) {
                                    System.out.println("Truck");
                                }else {
                                    System.out.println("Passenger car");
                                }
                            }
                            System.out.println("Number:" + (parking.placeList.get(i)/1000));
                            System.out.println("Parking time:"+(parking.placeList.get(i)%1000));
                        }
                        if((parking.placeList.get(n - 1).equals(parking.placeList.get(n - 2)))&(parking.placeList.get(n-1) != 0)){
                            System.out.println("Truck");
                            System.out.println("Number:" + (parking.placeList.get(n-1)/1000));
                            System.out.println("Parking time:"+(parking.placeList.get(n-1)%1000));
                        }else{
                            System.out.println("Passenger car:");
                            System.out.println("Number:" + (parking.placeList.get(n-1)/1000));
                            System.out.println("Parking time:"+(parking.placeList.get(n-1)%1000));
                        }

                        System.out.println();

                        for (int i = 0; i < k ; i++) {
                            System.out.println("Truck");
                            System.out.println("Number:" + (parking.placeBigList.get(i)/1000));
                            System.out.println("Parking time:"+(parking.placeBigList.get(i)%1000));
                        }
                        break;
                }
                System.out.println("Need another team or want to continue? There's a queue, it's not good to detain people)");
            }

            timeIsRunningOut(parking.placeBigList, k);
            timeIsRunningOut(parking.placeList, n);

            System.out.println("Do you need to close the Parking lot?");
            if (scan.next().equalsIgnoreCase("yes"))
                System.exit(0);
        }

    }

    public static void carsLeaving( ArrayList<Integer> list,  int l) {
        list.ensureCapacity(l);
        for (int i = 0; i < l; i++) {
            int f = list.get(i)%1000;
            if (f == 0) {
                list.set(i,0);
            }
        }
    }

    public static void timeIsRunningOut(ArrayList<Integer> list, int l) {
        for (int i = 0; i < l; i++) {
            if (list.get(i) != 0) {
                list.set(i, (list.get(i)-1));
            }
        }
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public static boolean placementOfCars(ArrayList<Integer> list, int count, Turnstile turnstile, Cars ob, boolean occupied) {
        int time;

        int noapMethod = numberOfAvailablePlace(list);
        if (count <= noapMethod) {
            for (int i = 0; i < count; i++) {
                turnstile.setNumber(ob.getNumber(turnstile.getNumber()));
                time = ob.getTime();

                list.set(i ,turnstile.getNumber() * 1000 + time);
            }
        } else {
            if (noapMethod == 0) {
                occupied = true;
            } else {
                for (int i = 0; i < noapMethod; i++) {
                    turnstile.setNumber(ob.getNumber(turnstile.getNumber()));
                    time = ob.getTime();

                    list.set(i,turnstile.getNumber() * 1000 + time);
                }
                occupied = true;
            }
        }
        return occupied;
    }

    public static int numberOfAvailablePlace(ArrayList<Integer> list) {
        int count = 0;
        Collections.sort(list);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == 0) {
                count++;
            } else {
                break;
            }
        }
        return count;
    }

    public static void appearanceParkingSpaces(ArrayList<Integer> list) {
        System.out.println("Will be through: ");
        int min = 11;
        for (int i = 0; i < list.size(); i++) {
            if ((list.get(i) % 1000) < min) {
                min = (list.get(i) % 1000);
            }
        }
        System.out.println(min);
        System.out.println();
    }

    public static void enter(ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            System.out.print("number: ");
            System.out.println(list.get(i)/1000);
            System.out.print("time: ");
            System.out.println(list.get(i) % 1000);
        }
    }
}
